# Manual Ethereum transaction signer

Manual Ethereum transaction signer in Raku.

## License

This is free and open source software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Credits

1. https://github.com/ethjs/ethjs-signer/blob/8739b3eb08714da5749175f3f5c1096e3046fe87/dist/ethjs-signer.js#L5705
2. https://gitlab.com/pheix/net-ethereum-perl6/-/issues/24
3. https://medium.com/mycrypto/the-magic-of-digital-signatures-on-ethereum-98fe184dc9c7
4. https://eips.ethereum.org/EIPS/eip-155
5. Y-parity: https://ethresear.ch/t/you-can-kinda-abuse-ecrecover-to-do-ecmul-in-secp256k1-today/2384/4

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
