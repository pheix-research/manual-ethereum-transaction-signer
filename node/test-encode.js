function encodeLength(len, offset) {
    if (len < 56) {
        return Buffer.from([len + offset])
    } else {
        const hexLength = intToHex(len)
        const lLength = hexLength.length / 2
        const firstByte = intToHex(offset + 55 + lLength)
        console.log("\thex len: " + hexLength + ", lLen: " + lLength + ", firstByte: " + firstByte);
        return Buffer.from(firstByte + hexLength, 'hex')
    }
}

function intToHex(integer) {
    if (integer < 0) {
        throw new Error('Invalid integer as argument, must be unsigned!')
    }
    const hex = integer.toString(16)
    return hex.length % 2 ? `0${hex}` : hex
}

function encode(input) {
    if (Array.isArray(input)) {
        const output = []
        for (let i = 0; i < input.length; i++) {
            output.push(encode(input[i]))
        }
        const buf = Buffer.concat(output)
        return Buffer.concat([encodeLength(buf.length, 192), buf])
    } else {
        const inputBuf = toBuffer(input)
        return inputBuf.length === 1 && inputBuf[0] < 128 ?
            inputBuf :
            Buffer.concat([encodeLength(inputBuf.length, 128), inputBuf])
    }
}

function intToBuffer(integer) {
    const hex = intToHex(integer)
    return Buffer.from(hex, 'hex')
}

function isHexPrefixed(str) {
    return str.slice(0, 2) === '0x'
}

function toBuffer(v) {
    if (!Buffer.isBuffer(v)) {
        if (typeof v === 'string') {
            if (isHexPrefixed(v)) {
                return Buffer.from(padToEven(stripHexPrefix(v)), 'hex')
            } else {
                return Buffer.from(v)
            }
        } else if (typeof v === 'number' || typeof v === 'bigint') {
            if (!v) {
                return Buffer.from([])
            } else {
                return intToBuffer(v)
            }
        } else if (v === null || v === undefined) {
            return Buffer.from([])
        } else if (v instanceof Uint8Array) {
            return Buffer.from(v)
        } else if (BN.isBN(v)) {
            // converts a BN to a Buffer
            return Buffer.from(v.toArray())
        } else {
            throw new Error('invalid type')
        }
    }
    return v
}

console.log(encode(['dog', 'raku', ['dog', 'raku']]));
