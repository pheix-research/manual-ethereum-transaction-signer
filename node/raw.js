const isHexPrefixed = require('is-hex-prefixed');
const rlp = require('rlp');
const BN = require('bn.js');
var Web3 = require('web3');
const web3 = new Web3('https://goerli.prylabs.net');
const keccak256 = require('keccak256');
var elliptic = require('elliptic');
const secp256k1 = new elliptic.ec('secp256k1');

function stripHexPrefix(str) {
    if (typeof str !== 'string') {
        return str;
    }

    return isHexPrefixed(str) ? str.slice(2) : str;
}

function numberToBN(arg) {
    var errorMessage = new Error('[number-to-bn] while converting number to BN.js object, argument "' + String(arg) + '" type "' + String(typeof(arg)) + '" must be either a negative or positive (1) integer number, (2) string integer, (3) valid prefixed hex number string, (4) BN.js object instance or a (5) bignumber.js object.');
    if (typeof arg === 'string') {
        if (arg.match(/0[xX][0-9a-fA-F]+/) || arg.match(/^-?[0-9]+$/)) {
            if (isHexPrefixed(arg)) {
                return new BN(stripHexPrefix(arg), 16);
            } else if (arg.substr(0, 3) === '-0x') {
                return new BN('-' + String(arg.slice(3)), 16);
            } else { // eslint-disable-line
                return new BN(arg, 10);
            }
        } else {
            throw errorMessage;
        }
    } else if (typeof arg === 'number') {
        return new BN(String(arg));
    } else if (typeof arg === 'object' &&
        arg.toString &&
        (!arg.pop && !arg.push)) {
        if (arg.toString(10).match(/^-?[0-9]+$/)) {
            if (arg.toArray && arg.toTwos) {
                return arg;
            } else {
                return new BN(arg.toString(10));
            }
        } else {
            throw errorMessage;
        }
    } else {
        throw errorMessage;
    }
}

function stripZeros(buffer) {
    var i = 0; // eslint-disable-line
    for (i = 0; i < buffer.length; i++) {
        if (buffer[i] !== 0) {
            break;
        }
    }
    return i > 0 ? buffer.slice(i) : buffer;
}

function padToEven(str) {
    return str.length % 2 ? '0' + str : str;
}

function bnToBuffer(bn) {
    return stripZeros(new Buffer(padToEven(bn.toString(16)), 'hex'));
}

var transactionFields = [{
    name: 'nonce',
    maxLength: 32,
    number: true
}, {
    name: 'gasPrice',
    maxLength: 32,
    number: true
}, {
    name: 'gasLimit',
    maxLength: 32,
    number: true
}, {
    name: 'to',
    length: 20
}, {
    name: 'value',
    maxLength: 32,
    number: true
}, {
    name: 'data'
}];

var raw = [];

console.log(web3.utils.toHex('3000000'));
console.log(web3.utils.toHex('8000000'));
console.log(web3.utils.toHex('1000000000'));

var transaction = {
    from: '0x39c5f60e8c41bcb81f6f25e35af4021b29ff99d3',
    to: '0xebf17dd3da582897ea80e151a931a2284032c837',
    gas: 3000000,
    gasLimit: 8000000,
    gasPrice: 1000000000,
    value: web3.utils.toHex('0'),
    data: '0x2b4399bc000000000000000000000000000000000000000000000000000000000000006000000000000000000000000000000000000000000000000000000000000000a00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000774657374746162000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000015232069643B646174613B636F6D7072657373696F6E0000000000000000000000',
    nonce: 1,
}

transactionFields.forEach(function(fieldInfo) {
    var value = new Buffer(0); // eslint-disable-line

    // shim for field name gas
    var txKey = fieldInfo.name === 'gasLimit' && transaction.gas ? 'gas' : fieldInfo.name;

    if (typeof transaction[txKey] !== 'undefined') {
        if (fieldInfo.number === true) {
            value = bnToBuffer(numberToBN(transaction[txKey]));
        } else {
            value = new Buffer(padToEven(stripHexPrefix(transaction[txKey])), 'hex');
        }
    }

    // Fixed-width field
    if (fieldInfo.length && value.length !== fieldInfo.length && value.length > 0) {
        throw new Error('[ethjs-signer] while signing raw transaction, invalid \'' + fieldInfo.name + '\', invalid length should be \'' + fieldInfo.length + '\' got \'' + value.length + '\'');
    }

    // Variable-width (with a maximum)
    if (fieldInfo.maxLength) {
        value = stripZeros(value);
        if (value.length > fieldInfo.maxLength) {
            throw new Error('raw transaction, invalid \'' + fieldInfo.name + '\' length, the max length is \'' + fieldInfo.maxLength + '\', got \'' + value.length + '\'');
        }
    }

    console.log(fieldInfo.name);
    console.log(value);

    raw.push(value);
});

//console.log(raw);

var rlp_encoded = rlp.encode(raw);
console.log(Buffer.from(rlp_encoded).toString('hex').match(/../g).join(' '));
console.log(keccak256(rlp_encoded));

var privateKey = '0x35b36dbae3fb76f3b1bdd2836af676347bc89cedf28276e925e7e7cdf3214952';
var signature = secp256k1.keyFromPrivate(new Buffer(privateKey.slice(2), 'hex')).sign(new Buffer(keccak256(rlp.encode(raw)), 'hex'), {
    canonical: true
});

console.log(signature);
